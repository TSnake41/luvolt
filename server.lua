#!/usr/bin/env luvit
local discordia = require "discordia"
local settings = require "settings"

local client = discordia.Client(settings.client)
local shared = settings.shared

local help = require "help"
local cmds = require "commands"
local pattern = require "pattern"

-- Build help.
help.make(require "help_info.lua")
shared.help = help

-- Initialize patterns.
pattern.init(shared)
shared.pattern = pattern

shared.comsg = {}
shared.client = client

client:on("ready", function ()
  print("Logged in as " .. client.user.username)
  client:setAFK(true)
end)

client:on("messageCreate", function (message)
  client:setAFK(false)

  if message.author == client.user then
    return -- Don't answer self messages.
  end

  if not pattern.iscommand(message.content) then
    -- Ignore messages that are not commands.
    return
  end

  local command = pattern.command(message.content)

  if cmds[command] then
    local status, err = pcall(cmds[command], message, message.channel, shared)
    if not status then
      if type(err) == "string" then
        -- Transform string (remove all path and only keep the file name)
        err = err:gsub("[%w/\\:]-(%w+%.lua)", "%1")
      end
      message.channel:send {
        embed = {
          title = "Erreur interne",
          description = tostring(err),
          color = 0xFF0000
        }
      }
    end
  end

  client:setAFK(true)
end)

-- Read token from token.txt
local token
do
  local token_file = io.open "token.txt"
  if token_file then
    -- Read the first line
    token = token_file:read "*l"
    -- Close file
    token_file:close()
  end
end

assert(token, "token.txt not found or invalid")

client:run("Bot " .. token)

client:setGame "luvit.io"
