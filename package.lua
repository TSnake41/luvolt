return {
  name = "luvolt",
  version = "1",
  luvi = { },
  files = {
    "**.lua"
  },
  dependencies = {
    "SinisterRectus/discordia",
    "luvit/json",
    "luvit/process",
    "luvit/require",
    "luvit/fs",
    "creationix/coro-http"
  }
}
