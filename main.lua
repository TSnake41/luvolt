-- Tiny bootstrap
unpack = unpack or table.unpack
table.unpack = unpack

local uv = require "uv"
_G.process = require "process".globalProcess()

local status, ret = xpcall(function ()
  require "./server.lua"
  uv.run()
end, debug.traceback)

if not status then
  io.stderr:write(ret)
else
  return ret or 0
end
