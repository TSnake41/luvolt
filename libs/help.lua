local help = {}

local format = string.format

function help.make(help_table, options)
  help.big_help = { fields = {} }
  help.pages = {}
  local fields = help.big_help.fields

  options = options or {}
  options.prefix = options.prefix or "%"

  local function add_field(name, value)
    fields[#fields + 1] = {
      name = options.prefix .. name,
      value = value,
      inline = false
    }
  end

  for k,v in pairs(help_table) do
    local interal_key = k:match "^#(%w+)"
    if interal_key then
      help.big_help[interal_key] = v
    else
      -- Handle command help
      local t = type(v)
      if t == "string" then
        -- Convert v to table.
        v = { text = v }
      end

      assert(type(v) == "table", "Unexpected help_info value.")

      if not v.hidden then
        -- Create help field
        add_field(k, v.text)
      end

      -- Create help page
      local lines = {}
      local args_str = ""

      if not v.nocmd then
        if v.args then
          --[[ Create args string (the string that contains all markdown
               formatted arguments). ]]
          local args_str_parts = {} -- Each markdown formatted argument.
          for i=1,#v.args do
            if v.args[i].optional then
              args_str_parts[i] = format("[%s]", v.args[i][1])
            else
              args_str_parts[i] = format("<%s>", v.args[i][1])
            end
            args_str = table.concat(args_str_parts, " ")
          end
        end
      end

      -- Create each line for initial command and each alias.
      local cmd_names = { k, unpack(v.alias or {}) }
      for i=1,#cmd_names do
        -- Create the line for the command name + args.
        lines[#lines + 1] = format("**%s%s** %s",
          options.prefix, cmd_names[i], args_str)
      end

      if v.args then
        -- Create an empty line.
        lines[#lines + 1] = ""

        -- Add argument(s) info
        for i=1,#v.args do
          -- Create the line for the argument.
          lines[#lines + 1] =
            format("**__%s__ :** %s", v.args[i][1], v.args[i][2])
        end
      end

      -- Create an empty line.
      lines[#lines + 1] = ""

      -- Add extended help (or basic if extended help is not available)
      lines[#lines + 1] = v.expanded or v.text

      if v.example then
        -- Include the example

        -- First, create an empty line.
        lines[#lines + 1] = ""

        -- Then add example string.
        lines[#lines + 1] = format("Example : %s", v.example)
      end

      local title_format = v.nocmd
        and "Luvolt help : __%s__ page"
        or "Luvolt help : __%s__ command"

      local help_embed = {
        title = title_format:format(k),
        description = table.concat(lines, "\n"),
        color = 0x0594BD
      }

      -- Include pages in db
      for _,cmd in pairs { k, unpack(v.alias or {}) } do
        help.pages[cmd] = help_embed
      end
    end
  end
end

return help
