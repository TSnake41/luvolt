local pattern = {}

local lower = string.lower
local prefix, prefix_size

function pattern.init(shared)
  -- Initialize prefix_size
  prefix, prefix_size = shared.prefix, #shared.prefix
end

function pattern.iscommand(str)
  local i = str:find(prefix, 1, true)
  return i == 1
end

-- Remove the prefix
local function unprefix(str)
  -- Remove spaces at the beginning then the prefix
  return str:match "%s*(.+)":sub(prefix_size + 1)
end

-- Get the command part of a string
function pattern.command(str)
  local cmd = unprefix(str):match "%S+"
  return cmd and lower(cmd)
end

function pattern.args_str(str)
  return unprefix(str):match("%S+%s+(.+)")
end

function pattern.args(str, i)
  -- Take all the arguments then split in tokens
  local tokens = (pattern.args_str(str) or ""):gmatch "%S+"

  -- Ignore (i) tokens
  for _=1,i or 0 do
    tokens() -- Ignore token
  end

  local args = {}
  -- Add remaining tokens to args table
  for arg in tokens do
    args[#args + 1] = arg
  end

  return args
end

return pattern
