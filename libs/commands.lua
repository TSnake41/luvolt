local cmd = {}

local doge = require "commands/doge"
local joke = require "commands/joke"
local sandbox = require "commands/sandbox"
local ttt = require "commands/ttt"
local comsg = require "commands/comsg"

cmd.help = require "commands/help"
cmd.hlp = cmd.help

-- Basic command
cmd.hello = require "commands/hello"
cmd.hi = cmd.hello
cmd.salut = cmd.hello

-- Random number generation commands
cmd.rand = require "commands/rand"
cmd.random = cmd.rand

-- Joke commands
cmd.joke = joke.no_joke
cmd._joke = joke.do_joke

-- Sandbox commands
cmd.lua = sandbox
cmd.sandbox = sandbox

-- Doge commands
cmd.doge = doge.say_doges

cmd.isdown = require "commands/isdown"

-- Tic-Tac-Toe commands
cmd["ttt.new"] = ttt.new
cmd["ttt.undo"] = ttt.undo
cmd["ttt.remove"] = ttt.undo
cmd["ttt.list"] = ttt.list
cmd["ttt.info"] = ttt.info

-- comsg functions
cmd["comsg.info"] = comsg.info
cmd[">"] = comsg.resume

-- Status functions
cmd.status = require "commands/status"

cmd.youclick = require "commands/youclick"

cmd.time = require "commands/time"
cmd.date = cmd.time

cmd.servers = require "commands/servers"
cmd.randompic = require "commands/randompic"

return cmd
