local help = {}

help["#title"] = "Help"
help["#description"] = "Luvolt help"
help["#color"] = 0x0594BD
help["#description"] = "**%help [commande]** permet d'avoir une aide détaillée sur une commande."

help.help = {
  args = { { "command", "Target command", optional = true } },
  text = "Shows this",
  expanded = "Shows luvolt help, you can specify a argument to get expanded help on a command.",
  alias = { "hlp" }
}

help.joke = "Essaye de raconter une blague"

help.hello = {
  text = "Dis bonjour",
  alias = { "hi", "salut" }
}

help.rand = {
  args = {
    { "min", "Nombre aléatoire minimum", optional = true },
    { "max", "Nombre aléatoire maximum", optional = true }
  },
  text = "Donne un nombre aléatoire",
  expanded = "Donne un nombre aléatoire situé entre min et max.",
  alias = { "random" }
}

help.lua = {
  args = { { "code", "Code à exécuter dans la sandbox" } },
  text = "Exécute arbitrairement du code Lua",
  expanded = table.concat({
    "Exécute arbitrairement du code Lua.",
    "En cas d'erreur lors de la compilation ou l'exécution, retourne le message d'erreur.",
    "Sinon, si l'exécution est réussie, renvoie les valeurs de retour et la sortie stdout."
  }, "\n"),
  alias = { "sandbox" },
  example = '```lua print "Hello World !"```'
}

help.doge = {
  args = { { "count", "Nombre de phrase à dire." } },
  text = "Crée des phrases inspirés du mème Doge",
  example = "```doge 3```"
}

help._joke = {
  text = "Racconte réellement une blague _(chuuut, c'est un secret)_.",
  hidden = true
}

help["ttt.*"] = {
  text = "Voir `help ttt`",
  expanded = table.concat({
    "Liste des commandes du Tic-Tac-Toe : ",
    " • **__ttt.new__**",
    " • **__ttt.remove__**",
    " • **__ttt.list__**"
  }, "\n"),
  alias = { "ttt" },
  nocmd = true
}

help["ttt.new"] = {
  args = {
    { "J1", "Joueur 1" },
    { "J2", "Joueur 1" },
  },
  text = "Crée une nouvelle partie",
  expanded = table.concat ({
    "Crée une nouvelle partie, les deux joueurs de cette partie sont J1 et J2.",
    "Les joueurs données en arguments doivent être mentionnées.",
    "Pour jouer, les joueurs doivent utiliser la commande spéciale '>'",
  }, "\n"),
  example = "```ttt.new @Joueur1 @Joueur2``````> msg```",
  hidden = true
}

help["ttt.list"] = {
  expanded = table.concat ({
    "Obtient la liste des parties en cours avec les informations sur ces parties.",
    "**__Réservé à l'administrateur du bot__**"
  }, "\n"),
  hidden = true
}

help["ttt.undo"] = {
  args = { { "ID", "ID de la partie à annuler" } },
  expanded = table.concat ({
    "Permet d'annuler une partie en cours.",
    "Utilisé tel quel, il permet de mettre fin à la partie auquel on assiste.",
    "Utilisé avec un ID de partie, il permet d'annuler une partie (**__Réservé à l'administrateur du bot__**)"
  }, "\n"),
  alias = { "ttt.remove" },
  hidden = true
}

help.about = {
  text = "**Luvolt** is a Discord bot made by <@179685260001411072>.",
  hidden = true,
  nocmd = true
}

-- "pages" page generator
local help_list = { "Complete list of Luvolt commands" }
for k,v in pairs(help) do
  if k:sub(1,1) ~= "#" then
    local cmd_names = { k, unpack(v.alias or {}) }
    for i=1,#cmd_names do
      local l = string.format(" • **__%s__**", cmd_names[i])

      if v.alias and cmd_names[i] ~= k then
        l = l .. string.format(" _(__%s__'s alias)_", k)
      end

      help_list[#help_list + 1] = l
    end
  end
end

help.cmd_list = {
  hidden = true,
  nocmd = true,
  expanded = table.concat(help_list, "\n"),
  alias = { "list", "pages" }
}

return help
