local discordia = require "discordia"

local function read_file(path)
  local f = io.open(path)
  if f then
    local content = f:read "*a"
    f:close()
    return content
  end
end

return function (message, channel, shared)
  local temp = read_file "/sys/class/thermal/thermal_zone0/temp"
  if temp then
    temp = tonumber(temp) / 1000
  end

  local stats = {
    string.format("temp: %.1f°C", temp),
  }

  channel:send {
     content = nil,
     embed = {
       description = table.concat(stats, "\n"),
       title = "luvolt status",
     }
   }
end
