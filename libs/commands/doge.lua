local prefixes, suffixes = { "many", "such", "so", "much", "very" }, {
  "wow", "luvit", "lua", "luv", "noot", "speed", "fast", "minimalism",
  "clearness", "simplicity", "simple", "nil", "dishwasher", "fengari"
}

local random = math.random

local function generate(prefix, suffix)
  if not prefix then
    prefix = prefixes[random(1, #prefixes)]
  end

  if not suffix then
    suffix = suffixes[random(1, #suffixes)]
  end

  if suffix == "wow" then
    -- Add a little chance that "wow" does not have a prefix.
    prefix = random(1, 4) == 1 and "" or prefix
  end

  return string.format("%s %s", prefix, suffix)
end

local function say_doges(message, channel)
  local count = tonumber(message.content:match "[+-]?%d+")

  if count then
    if count > 5 then
      channel:send(generate(nil, "messages"))
    elseif count <= 0 then
      channel:send(generate(nil, "negative"))
    else
      for _=1,count do
        channel:send(generate())
      end
    end
  else
    channel:send(generate(nil, "syntax"))
  end
end

return {
  prefixes = prefixes,
  suffixes = suffixes,
  generate = generate,
  say_doges = say_doges
}
