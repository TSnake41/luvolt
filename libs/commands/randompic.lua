local http = require "coro-http"
local json = require "json"

return function (message, channel, shared)
  local alnum = "0123456789abcdefghijklmnopqrstuvwxyz"

  local id = ""

  for i=1,6 do
    local i = math.random(1, #alnum)
    id = id .. alnum:sub(i, i)
  end

  channel:send(string.format("https://prnt.sc/%s", id))
end
