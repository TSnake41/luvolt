local jokes = {}

local random = math.random

local random_jokes = {
  [[Il existe qu'une seule vache comique.
**La vache qui rit !**
  ]], [[Tout le monde se demande pourquoi vous devez cliquer sur Youtube.
**Car you clique !**
  ]], [[Quelle est la différence entre une grosse bonne femme et des pantoufles ?
**Bah y en a pas ! On est bien dedans, mais on sort pas avec ! Allez, Josiane, le prends pas mal, c’est pour la déconne.**
  ]], [[Quelle est la différence entre un courtier de Wall Street et un acteur porno ?
**Le courtier a des actions en bourse alors que l’autre a les bourses en action**
  ]]
}

function jokes.no_joke(message, channel)
  channel:send {
    content = nil,
    embed = {
      description = "On m'a fait oublier les blagues comme elles étaient gènantes. :upside_down:"
    }
  }
end

function jokes.do_joke(message, channel)
  local joke_index = random(1, #random_jokes)
  channel:send {
    content = random_jokes[joke_index],
    embed = {
      description = "C'était une blague de Luvolt !",
      color = random(0x000000, 0xFFFFFF)
    }
  }
end

return jokes
