local random = math.random

return function (message, channel)
  local min, max = message.content:match "([-+]?%d+).-([-+]?%d+)"

  local n = (min and max) and random(min, max) or random()

  channel:send {
    content = nil,
    embed = {
      description = tostring(n),
      color = random(0x000000, 0xFFFFFF)
    }
  }
end
