local http = require "coro-http"
local json=  require "json"
local format = string.format

local key = "03e6b292760bbd1c65c60294c618d96e98a87433"

local function count(message, channel)
  local res, body = http.request("GET", "https://youclick.fr/api/?analyzes&key=" .. key)
  body = body:gsub("%s", "")
  channel:send(format("<@%s>, Youclick a actuellement %s vidéos analysés :O", message.author.id, body))
end

local function top(message, channel)
  local res, body = http.request("GET", "https://youclick.fr/api/?top&key=" .. key)
  local t = json.decode(body)

  local new_t = {}
  for k,v in pairs(t) do
    new_t[tonumber(k)] = v
  end
  t = new_t

  local embeds = {}

  for i=1,#t do
    local vid = t[i]
    channel:send {
      embed = {
        title = vid.title,
        description = format("**__SCORE__** : **%s**", vid.score),
        thumbnail = {
          url = format("https://img.youtube.com/vi/%s/0.jpg", vid.id)
        },
        color = 0xFF0000,
        author = {
          name = format("TOP %s : %s", i, vid.auteur)
        },
        url = "https://www.youtube.com/watch?v=" .. vid.id
      }
    }
  end
end

local func_table = {
  count = count,
  top = top
}

return function (message, channel, shared)
  local f_str = shared.pattern.args_str(message.content)

  local f = func_table[f_str]
  if not f then
    channel:send(format("'%s' est une commande putaclic non ?", f_str))
    return
  end

  f(message, channel, shared)
end
