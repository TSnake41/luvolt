local comsg = {}
local format = string.format

function comsg.info(message, channel, shared)
  local fields = {}

  for k,v in pairs(shared.comsg) do
    fields[#fields] = {
      name = format("user : <@%s>", k),
      value = coroutine.status(v),
      inline = false
    }
  end

  channel:send {
    embed = {
      title = "Coroutines en cours d'exécution",
      fields = fields
    }
  }
end

function comsg.resume(message, channel, shared)
  local msg = shared.pattern.args_str(message.content)
  local co = shared.comsg[message.author.id]

  if co then
    coroutine.resume(co, msg, channel, shared)
  else
    channel:send {
      embed = {
        description = "Aucune coroutine active",
        color = 0x1655DF
      }
    }
  end
end

return comsg
