return function (message, channel, shared)
  for id,guild in pairs(shared.client.guilds) do
    local desc = {}
    for _,v in ipairs {
      "owner_id",
      "splash",
      "region",
      "verification_level",
      "member_count"
    } do
      desc[#desc + 1] = string.format("%s = %s", v, tostring(guild[v]))
    end

    channel:send {
      content = nil,
      embed = {
        description = table.concat(desc, '\n'),
        title = guild.name,
        image = {
          url = guild.iconURL
        }
      }
    }
  end
end
