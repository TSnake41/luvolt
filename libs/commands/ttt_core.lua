local ttt_core = {}
local format = string.format
local patterns = {
  -- Lines
  { 1, 2, 3 },
  { 4, 5, 6 },
  { 7, 8, 9 },
  -- Columns
  { 1, 4, 7 },
  { 2, 5, 8 },
  { 3, 6, 9 },
  -- Diagonals
  { 1, 5, 9 },
  { 3, 5, 7 }
}

local games = {}

ttt_core.games = games

local function check_board(t)
  for i=1,#pattenrs do
    local p = patterns[i]
    -- Check if all the cases have the same player
    local player = t[p[1]]
    -- If player == nil then there is no player at this case
    if player then
      local same = true
      for i=2,#p do
        if player ~= t[p[i]] then
          same = false
        end
      end

      if same then
        return player
      end
    end
  end
end

local function userid_to_str(id)
  return id and format("<@%s>", id) or "IA"
end

local function game_co(main_channel, game)
  do -- rock-paper-scissors
    game.state = "beginning"
    local names = {
      "Ciseaux",
      "Feuille",
      "Pierre"
    }

    local function display_choice(channel, choice)
      channel:send(format('Vous avez choisis "%s"', names[choice]))
    end

    main_channel:send "**[P]**ierre, **[F]**euille, **[C]**iseaux !\nConseil : Envoyez des messages privés à luvolt."
    ::retry::

    local choices = {}
    -- rock = 1, paper = 2, scissors = 3
    repeat
      local msg, channel, player_id = coroutine.yield()
      if msg:match "^%s*[cC]" then
        choices[player_id] = 1
        display_choice(channel, 1)
      elseif msg:match "^%s*[fF]" then
        choices[player_id] = 2
        display_choice(channel, 2)
      elseif msg:match "^%s*[pP]" then
        choices[player_id] = 3
        display_choice(channel, 3)
      else
        channel:send "Choix invalide !"
      end
    until #choices ~= 2

    main_channel:send (
      format ("%s (%s) vs %s (%s)",
        names[choices[1]], userid_to_str(game.players[1]),
        names[choices[1]], userid_to_str(game.players[2])
      )
    )

    --[[
      Compare choices
      1 -> 2
      2 -> 3
      3 -> 1
    ]]

    if choices[1] == choices[2] then
      main_channel:send "Les choix sont identiques, on recommence !"
      goto retry
    end

    local win_id
    do
      local a, b = unpack(choices)

      win_id = (
        (a == 1 and b == 2) or
        (a == 2 and b == 3) or
        (a == 3 and b == 1)
      ) and 1 or 2
    end

    main_channel:send (
      format (
        "%s gagne ! Il commence le premier.",
        userid_to_str(game.players[win_id])
      )
    )

    game.player_turn = win_id
  end

  -- Game loop
  if game.destroy then
    game.destroy()
  end
end

function ttt_core.display(channel, game)
  if game.state == "beginning" then
    channel:send {
      title = "Plateau de jeu",
      color = 0x2266FF,
      description = "Tic-Tac-Toe en cours...",
      footer = {
        text = "id : " .. game.id
      }
    }
    return
  else
    local lines = {}
    local board = game.board

    for i=1,9,3 do
      lines[#lines + 1] = concat ({
        case_emojis[board[i] or 3],
        case_emojis[board[i + 1] or 3],
        case_emojis[board[i + 2] or 3]
      }, '')
    end

    lines[4] = format("\n%s%s", case_emojis[1], userid_to_str(game.players[1]))
    lines[5] = format("%s%s", case_emojis[2], userid_to_str(game.players[2]))

    local str = concat(lines, '\n')

    channel:send {
      embed = {
        title = "Plateau de jeu",
        color = 0x2266FF,
        description = str,
        footer = {
          text = "id : " .. game.id
        }
      }
    }
  end
end

local function player_co(game, player_id)
  return function ()
    while true do
      local msg, channel = coroutine.yield()
      if not game.player_turn or game.player_turn == player_id then
        coroutine.resume(game.main_co, msg, channel, player_id)
      end
    end
  end
end

function ttt_core.new(channel)
  --[[
    Returns 3 things :
     - game table
     - 2 coroutines (p1, p2)
      Both can be a coroutine or nil (if it is an AI)
  ]]
  local id
  for i=1,math.huge do
    if not games[i] then
      id = i
      break
    end
  end

  local game = {
    id = id,
    player_turn = false,
    board = {},
  }

  local main_co = coroutine.create(game_co)
  coroutine.resume(main_co, channel, game)

  game.main_co = main_co

  local co1, co2 = coroutine.create(player_co(game, 1)), coroutine.create(player_co(game, 2))

  coroutine.resume(co1)
  coroutine.resume(co2)

  print(co1, co2)

  return game, co1, co2
end

return ttt_core
