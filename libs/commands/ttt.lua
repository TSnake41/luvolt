local insert, remove, concat, format, random = table.insert, table.remove,
  table.concat, string.format, math.random

local ttt = {}
local ttt_core = require "./ttt_core"

local case_emojis = {
  ":red_circle:", -- P1
  ":large_blue_circle:", -- P2
  ":black_circle:" -- Nothing
}

function ttt.new(message, channel, shared)
  local client_id = shared.client.user.id
  local comsg = shared.comsg
  local args = shared.pattern.args_str(message.content)

  local p1, p2 = args:match "<@!?(%d+)>.*<@!?(%d+)>"

  channel:send {
    embed = {
      color = 0x2266FF,
      description =
        format("Création d'une partie avec <@%s> et <@%s>...", p1, p2)
    }
  }

  if not p1 or not p2 then
    channel:send {
      embed = {
        color = 0xFF0000,
        description = format("Utilisateurs invalides !")
      }
    }
    return
  end

  if comsg[p1] or comsg[p2] then
    channel:send {
      embed = {
        color = 0xFF0000,
        description = "L'un des joueur est occupé."
      }
    }
    return
  end

  local game, co1, co2 = ttt_core.new()

  game.players = { p1, p2 }
  game.destroy = function ()
    -- Remove all variables
    comsg[p1] = nil
    comsg[p2] = nil

    ttt_core.games[id] = nil
  end

  ttt_core.games[game.id] = game

  comsg[p1], comsg[p2] = co1, co2
end

function ttt.undo(message, channel, shared)
  local args = shared.pattern.args(message.content)
end

function ttt.list(message, channel, shared)
  local client_id = shared.client.user.id
  local lines = {}

  local function format_id(pl)
    pl = pl == -1 and client_id or pl

    local str = format("<@%s>", pl)
    if pl == client_id then
        str = str .. " (IA)"
    end
    return str
  end

  for k,v in pairs(ttt_core.games) do
    local p1, p2 = unpack(v.players)

    lines[#lines + 1] = format("[%d] : %s vs %s", k, format_id(p1), format_id(p2))
  end

  channel:send {
    embed = {
      title = "Luvolt tic-tac-toe games",
      description = concat(lines, '\n')
    }
  }
end

function ttt.info(message, channel, shared)
  local id = tonumber(shared.pattern.args_str(message.content))
  local games = ttt_core.games
  if games[id] then
    ttt_core.display(channel, games[id])
  else
    channel:send("Aucune partie trouvée pour id = " .. id)
  end
end

function ttt.test(message, channel, shared)
  display(channel, {
    1, nil, 2,
    1, 2, nil,
    nil, 2, 1
  })
end

return ttt
