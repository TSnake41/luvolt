local http = require "coro-http"
local random, format = math.random, string.format

local names = {
  "Gerard",
  "Edgard",
  "Patrick",
  "Steeve",
  "Michel",
  "Kim",
  "Staline",
  "Hitler",
  "Loïc",
  "TSnake41",
  "Trump",
  "Jammy"
}

return function (message, channel, shared)
  local url = shared.pattern.args_str(message.content)
  if not url:match "^https?://.+" then
    url = "http://" .. url
  end

if pcall(function () http.request("GET", url) end) then
    channel:send(format("Hé %s, le site n'est pas down !", names[random(1, #names)]))
  else
    channel:send(format("Hé %s, les admins de ce site font de la merde, ou pire, tu me dis de la merde !", names[random(1, #names)]))
  end
end
