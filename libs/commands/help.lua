return function (message, channel, shared)
  local name = shared.pattern.args_str(message.content)
  if name then
    if shared.help.pages[name] then
      channel:send {
        content = nil,
        embed = shared.help.pages[name]
      }
    else
      channel:send(string.format("Page '%s' non trouvée.", name))
    end
  else
    channel:send {
      content = nil,
      embed = shared.help.big_help
    }
  end
end
