local sandbox = {}

local function light_dup(t)
  local new_t = {}
  for k,v in pairs(t) do
    new_t[k] = v
  end

  return new_t
end

local concat, format = table.concat, string.format

return function (message, channel, shared)
  local code = shared.pattern.args_str(message.content)
  if not code then
    channel:send {
      content = nil,
      embed = {
        title = "Unable to run code",
        description = "No code",
        color = 0xFF0000
      }
    }
    return
  end

  local stdout_lines = nil

  local function print_discord(...)
    if not stdout_lines then
      stdout_lines = { "__**stdout**__\n--------```" }
    end

    local t = table.pack(...)
    for i=1,t.n do
      t[i] = tostring(t[i])
    end

    stdout_lines[#stdout_lines + 1] = concat(t, "\t")
  end

  local env = {
    math = light_dup(math), string = light_dup(string),
    table = light_dup(table),  bit = light_dup(bit),
    tostring = tostring, tonumber = tonumber,
    print = print_discord, assert = assert,
    error = error, pairs = pairs, ipairs = ipairs,
    os = { clock = os.clock, date = os.date,
    difftime = os.difftime, time = os.time },
    type = type, coroutine = light_dup(coroutine)
  }

  env._G = env

  local f, err = load(code)

  if not f then
    channel:send {
      embed = {
        title = "Unable to run code",
        description = err,
        color = 0x882288
      }
    }
    return
  end

  channel:send {
    embed = {
      title = "Running",
      description = format("```lua\n%s```", code),
      color = 0x5555DD
    }
  }

  setfenv(f, env)
  local ret = { pcall(f) }
  local status, err = ret[1], ret[2]

  -- Remove status from ret (returned values)
  table.remove(ret, 1)

  if status then
    local fields = {}
    for i=1,#ret do
      local v = ret[i]
      local new_field = {
        name = format("[%d]", i),
        value = format("type: %s\nvalue: %s", type(v), tostring(v))
      }
      fields[i] = new_field
    end

    channel:send {
      embed = {
        description = "__**Sucessful !**__",
        fields = fields,
        color = 0x11FF44
      }
    }

    if stdout_lines then
      stdout_lines[#stdout_lines + 1] = "```"

      channel:send {
        embed = {
          description = concat(stdout_lines, "\n"),
          color = 0x222222
        }
      }
    end
  else
    channel:send {
      embed = {
        title = "Error during execution",
        description = tostring(err),
        color = 0xCC0000
      }
    }
  end
end
